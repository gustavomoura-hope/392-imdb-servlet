package edu.hope.cs.csci392.imdb.model;

public class Movie {
	private int movieID;
	private String movieTitle;	
	private int year;
	private int runningTimeInMinutes;
	private String primaryGenre;
	
	public Movie (
		int movieID, String movieTitle, int year, 
		int lengthInMinutes, String primaryGenre
	) {
		this.movieID = movieID;
		this.movieTitle = movieTitle;
		this.year = year;
		this.runningTimeInMinutes = lengthInMinutes;
		this.primaryGenre = primaryGenre;
	}

	public int getMovieID() {
		return movieID;
	}

	public String getMovieTitle() {
		return movieTitle;
	}

	public int getYear() {
		return year;
	}

	public int getLengthInMinutes() {
		return runningTimeInMinutes;
	}

	public String getPrimaryGenre() {
		return primaryGenre;
	}
}